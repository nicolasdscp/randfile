package main

import (
	"github.com/alexflint/go-arg"
)

var CLI struct {
	FileName  string `arg:"positional,required" help:"target file name"`
	Size      int    `arg:"positional" help:"size of generated file (in byte by default). Lower unit have more priority"`
	Byte      bool   `arg:"-b" help:"size in B"`
	Kilo      bool   `arg:"-k" help:"size in kB"`
	Mega      bool   `arg:"-M" help:"size in MB"`
	Giga      bool   `arg:"-G" help:"size in GB"`
	Overwrite bool   `arg:"-o" help:"Overwrite file if he already exist"`
}

func initCLI() {
	CLI.Size = 1
	CLI.Byte = true
	arg.MustParse(&CLI)
}
