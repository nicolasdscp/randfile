package main

import (
	"bufio"
	"errors"
	"math/rand"
	"os"
)

// BUFFER_SIZE define writing buffer size
const BUFFER_SIZE = 16384

// createFile parse privilege and open file
func createFile() {
	file, err := prepareFile()
	if err != nil {
		panic(err)
	}
	writeContent(file)
	_ = file.Close()
}

// checkFileExist check if file already exist
func checkFileExist() (bool, error) {
	infos, err := os.Stat(CLI.FileName)
	if !os.IsNotExist(err) {
		if infos.IsDir() {
			return false, errors.New("given path cannot be a directory")
		}
		if !CLI.Overwrite {
			return true, errors.New("cannot overwrite file without giving -o flag")
		}
		return true, nil
	}
	return false, nil
}

// calcFinalSize calculate final size of file
func calcFinalSize() int64 {
	size := int64(CLI.Size)
	switch {
	case CLI.Kilo:
		size *= 1000
	case CLI.Mega:
		size *= 1000000
	case CLI.Giga:
		size *= 1000000000
	}
	return size
}

// writeContent write content in file
func writeContent(file *os.File) {
	var content []byte
	writer := bufio.NewWriter(file)
	finalSize := calcFinalSize()
	localBufferSize := int64(0)

	for finalSize > 0 {
		if finalSize < BUFFER_SIZE {
			localBufferSize = finalSize
		} else {
			localBufferSize = BUFFER_SIZE
		}
		content = make([]byte, localBufferSize)
		rand.Read(content)
		count, writeErr := writer.Write(content)
		if writeErr != nil {
			panic(writeErr)
		}
		finalSize -= int64(count)
		if flushErr := writer.Flush(); flushErr != nil {
			panic(flushErr)
		}
	}
}

// prepareFile prepare target file
func prepareFile() (*os.File, error) {
	exist, err := checkFileExist()
	if err != nil {
		return nil, err
	}
	if exist {
		if err := os.Remove(CLI.FileName); err != nil {
			return nil, err
		}
	}
	return os.Create(CLI.FileName)
}
